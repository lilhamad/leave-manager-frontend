# LeaveManager

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

# How to run
click https://leave-managerr.firebaseapp.com/

# Login Accounts
You can sign up or Log in with the following accounts

Admin login

- email: admin@admin.com
- password:admin@1234

Non-admin login

- email:admin@admin.com11
- password: admin@1234    

Only admin account will be able to update a leave requests

Non-admin will only be able to apply for a leave and see his/her leave status

# Features on the app
- Login
- Sign up
- Apply for a leave
- Update a leave(admin only)
- View Leave status
- View Users(admin only)



## Running the app locally -->

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
