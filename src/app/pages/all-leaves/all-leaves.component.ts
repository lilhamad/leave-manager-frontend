import { Component, OnInit } from '@angular/core';
import { LeaveService } from 'src/app/services/leave.service';
import * as moment from 'moment'

@Component({
  selector: 'app-all-leaves',
  templateUrl: './all-leaves.component.html',
  styleUrls: ['./all-leaves.component.css']
})
export class AllLeavesComponent implements OnInit {
  public leaves: any[];
  public message: string;
  public end_date: string;
  public start_date: string;
  public status: number;
  public id: number;
  public index: number;
  public show: boolean;
  public Status: any[];
  constructor(public leaveService: LeaveService,) { 
    
  }
  
  
  async ngOnInit() {
    await this.leaveService.getAllLeavesFromApi();
    this.leaves = await this.leaveService.getAllLeaves();
    this.Status = await this.leaveService.getLeaveStatus();
    await setTimeout(() => {
      
    }, 2000);
  }
  
  editLeave(index){
    this.index = index;
    this.show = true;
    this.start_date = this.leaves[index].start_date;
    this.end_date = this.leaves[index].end_date;
    this.status = this.leaves[index].status;
    this.id = this.leaves[index].id;
  }
  
  
  
  async updateLeave() {
    if (this.start_date!=null && this.end_date!=null) {
      await this.leaveService.updateLeaveFromApi({"start_date":this.start_date,"end_date":this.end_date, "status":this.status, "id":this.id})
      .then(async res => {
        this.leaves[this.index].status = this.status;
        this.leaveService.setAllLeaves(this.leaves)
        // this.router.navigate(['/home']);
      })
      .catch(async error => {
        // this.status = true;
        // this.message = error.error.email? error.error.email:'';
        // this.message += error.error.phone? 'Enter a valid phone number.':'';
      });
    } else {
      // this.status = true;
      // this.message = 'Please fill the form properly.'
    }
  }
  
}

