import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public email: string = "";
  public password: string = "";
  message: string;
  status: boolean;
  constructor(public userService: UserService, private router: Router,) { }
  
  ngOnInit() {
  }
  
  async login() {
    if (this.email!=null && this.password!=null) {
      await this.userService.getUserFromApi({"email":this.email, "password":this.password})
      .then(async res => {
        window.location.href = "/home"
        // this.router.navigate(['/home']);
      })
      .catch(async error => {
        this.status = true;
        this.message = error.error.error;
      });
    } else {
      this.status = true;
      this.message = 'Please fill the phone number and password appropriately.'
    }
  }
  
}
