import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import * as moment from 'moment'
import { LeaveService } from 'src/app/services/leave.service';

@Component({
  selector: 'app-create-leave',
  templateUrl: './create-leave.component.html',
  styleUrls: ['./create-leave.component.css']
})
export class CreateLeaveComponent implements OnInit {
  public end_date: string;
  public start_date: string;
  public id: string;
  message: string;
  status: boolean;
  constructor( private api: ApiService,
    public router: Router,
    public leaveService: LeaveService,) { }
    
    ngOnInit() {
    }
    
    async createLeave() {
      let start = moment(this.start_date);
      let end = moment(this.end_date);
      console.log(end.diff(start, 'days'));// 
      if(end.diff(start, 'days') > 7){
        this.status = false;
        if (this.start_date!=null && this.end_date!=null) {
          await this.leaveService.createLeaveFromApi({"start_date":this.start_date,"end_date":this.end_date, "application_date":moment().format('YYYY-MM-DD')})
          .then(async res => {
            this.router.navigate(['/home']);
          })
          .catch(async error => {
            this.status = true;
            this.message = error.error.email? error.error.email:'';
            this.message += error.error.phone? 'Enter a valid phone number.':'';
          });
        } else {
          this.status = true;
          this.message = 'Please fill the form properly.'
        }
      }else {
        this.status = true;
        this.message = 'Starting date and End date must at  up to 7 days in between'
      }
      
    }
    
  }