import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  public employees: any[];
  public is_admin: string;
  public name: string;
  public message: string;
  public status: number;
  public id: number;
  public index: number;
  public show: boolean;
  public type:any[] = [{false:"Normal"}, {true:"Admin"}]
  constructor(public userService: UserService,) { 
    this.employees = this.userService.getAllUser();
  }
  
  
  async ngOnInit() {
    if(!this.employees)
    await this.userService.getAllUserFromApi();
    this.employees = await this.userService.getAllUser();
    await setTimeout(() => {
      
    }, 2000);
  }
  
  editEmployee(index){
    this.index = index;
    this.show = true;
    this.name = this.employees[index].first_name;
    this.is_admin = this.employees[index].is_admin;
    this.id = this.employees[index].id;
  }
  
  
  
  async updateEmployee() {
  //   if (this.start_date!=null && this.end_date!=null) {
  //     await this.leaveService.updateEmployeeFromApi({"start_date":this.start_date,"end_date":this.end_date, "status":this.status, "id":this.id})
  //     .then(async res => {
  //       this.leaves[this.index].status = this.status;
  //       this.leaveService.setAllEmployees(this.leaves)
  //       // this.router.navigate(['/home']);
  //     })
  //     .catch(async error => {
  //       // this.status = true;
  //       // this.message = error.error.email? error.error.email:'';
  //       // this.message += error.error.phone? 'Enter a valid phone number.':'';
  //     });
  //   } else {
  //     // this.status = true;
  //     // this.message = 'Please fill the form properly.'
  //   }
  }
  
}

