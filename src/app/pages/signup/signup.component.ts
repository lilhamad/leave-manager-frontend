import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  public first_name: string;
  public last_name: string;
  public email: string;
  public phone: string;
  public password: string;
  message: string;
  status: boolean;
  constructor(    private api: ApiService,
    public router: Router,
    public userService: UserService,) { }

  ngOnInit() {
  }

  async signup() {
    if (this.email!=null && this.password!=null) {
      await this.userService.createUserFromApi({"first_name":this.first_name,"last_name":this.last_name, "email":this.email, "phone":this.phone, "password":this.password})
      .then(async res => {
        window.location.href = "/home"
        // this.router.navigate(['/home']);
      })
      .catch(async error => {
        this.status = true;
        this.message = error.error.email? error.error.email:'';
        this.message += error.error.phone? 'Enter a valid phone number.':'';
      });
    } else {
      this.status = true;
      this.message = 'Please fill the form properly.'
    }
  }

}

