import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { AllLeavesComponent } from './pages/all-leaves/all-leaves.component';
import { CreateLeaveComponent } from './pages/create-leave/create-leave.component';
import { EmployeesComponent } from './pages/employees/employees.component';


const routes: Routes = [

  { path: "login", component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'home', component: DashboardComponent },
  { path: "welcome", component: WelcomeComponent },
  { path: "create-leave", component: CreateLeaveComponent },
  { path: "all-leave", component: AllLeavesComponent },
  { path: "employees", component: EmployeesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
