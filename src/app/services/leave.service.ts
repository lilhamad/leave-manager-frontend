import { Injectable } from '@angular/core';
import { StoringService, dbTables } from './storing.service';
import { ApiService } from './api.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class LeaveService {

  constructor(private storage: StoringService,
     private api: ApiService,
     private userService: UserService,
     ) { }

  // #leaves
  async setMyLeaves(leaves) {
    await this.storage.set(dbTables.MY_LEAVES, JSON.stringify(leaves));
  }
   
  getMyLeaves() {
    let leaves = this.storage.get(dbTables.MY_LEAVES);
    return JSON.parse(leaves);
  }

  getLeaveStatus() {
    return [{name: 'Pending'},  {name: 'Approved'},  {name: 'Running'},  {name: 'Returned'},  {name: 'Cancelled'}, {name: 'Rejected' }]

  }

  async getMyLeavesFromApi():Promise<boolean> {
    return new Promise((resolve,reject) => {
      this.api.sendGet('leaves/'+this.userService.getUser().id).subscribe(async response => {
        await this.setMyLeaves(response);
        resolve(response);
      }, async (err) => {
        reject(err);
      });
    });
  }

  async createLeaveFromApi(leaveData):Promise<boolean> {
    leaveData["employee"] = this.userService.getUser().id
    return new Promise((resolve,reject) => {
      this.api.sendPost('create-leave',leaveData).subscribe(async response => {
        await this.setMyLeaves(response);
        resolve(response);
      }, async (err) => {
        reject(err);
      });
    });
  }

  async updateLeaveFromApi(leaveData):Promise<boolean> {
    return new Promise((resolve,reject) => {
      this.api.sendPut('leave/'+leaveData.id,leaveData).subscribe(async response => {
        await this.setMyLeaves(response);
        resolve(response);
      }, async (err) => {
        reject(err);
      });
    });
  }

  // all leaves
  async setAllLeaves(leaves) {
    await this.storage.set(dbTables.ALL_LEAVES, JSON.stringify(leaves));
  }
   
  getAllLeaves() {
    let leaves = this.storage.get(dbTables.ALL_LEAVES);
    return JSON.parse(leaves);
  }

  async getAllLeavesFromApi():Promise<boolean> {
    return new Promise((resolve,reject) => {
      this.api.sendGet('leaves').subscribe(async response => {
        await this.setAllLeaves(response);
        resolve(response);
      }, async (err) => {
        reject(err);
      });
    });
  }

}
