import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map, timeout } from 'rxjs/operators';

const user =  JSON.parse(localStorage.getItem('farm_user'));
let httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest'})
};

//staging
// const url ='http://localhost:8000/';
//Live
// const url = 'https://leave-manager.azurewebsites.net/'
const url = 'http://80.85.86.204:8000/'

const apiUrl = url+'api';

export enum ConnectionStatusEnum{
  ONLINE,
  OFFLINE
}

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  public  url = url;
  previousStatus: ConnectionStatusEnum = ConnectionStatusEnum.ONLINE;
  
  constructor(private http: HttpClient,) {
    
  }
  
  httpOptions (useToken: boolean, data?:any) {
    if (useToken ) {
      const user =  JSON.parse(localStorage.getItem('user'));
      const   token  = user.token;
      return {
        headers: new HttpHeaders({
          'Accept':'application/json',
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
          'Authorization': `Bearer ${token}`
        }),
        body: data? data :null
      };
    }
    return {
      headers: new HttpHeaders({
        'Accept':'application/json',
        'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest'
      })
    };
  }
  
  
  private handleError(error: HttpErrorResponse) {
    
    let errorMsg = null;
    if (error.error instanceof ErrorEvent) {
      errorMsg =  error.error.message;
    } else {
      if (error.status === 422) {
        errorMsg = error.error.errors;
      }
    }
    return errorMsg;
  }
  private extractData(res: Response) {
    const body = res;
    return body || { };
  }
  
  sendGet(endpoint, useToken?: boolean): Observable<any> {
    const url = `${apiUrl}/${endpoint}`;
    return this.http.get(url, this.httpOptions(useToken)).pipe(timeout(1000*60))            
  }
  
  sendPost(endpoint, data, useToken?: boolean): Observable<any> {
    const url = `${apiUrl}/${endpoint}`;
    return this.http.post(url, data, this.httpOptions(useToken)).pipe(timeout(1000*60));
  }
  
  sendDelete(endpoint, data, useToken?: boolean): Observable<any> {
    const url = `${apiUrl}/${endpoint}`;
    return this.http.delete(url,  this.httpOptions(useToken,data)).pipe(timeout(1000*60));
  }
  
  sendPut(endpoint: string, data, useToken?: boolean): Observable<any> {
    const url = `${apiUrl}/${endpoint}`;
    return this.http.put(url, data, this.httpOptions(useToken)).pipe(timeout(1000*60));
  }
  
}

