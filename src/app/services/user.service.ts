import { Injectable } from '@angular/core';
import { StoringService, dbTables } from './storing.service';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private storage: StoringService,private api: ApiService) { }

   
  async setUser(user) {
    await this.storage.set(dbTables.USER, JSON.stringify(user));
  }
   
  getUser() {
    let user = this.storage.get(dbTables.USER) || "";
    if(user != "" && user != "undefined"){
      return JSON.parse(user);
    }
    
  }

   
  async setAllUser(user) {
    await this.storage.set(dbTables.ALL_USERS, JSON.stringify(user));
  }
   
  getAllUser() {
    let user = this.storage.get(dbTables.ALL_USERS) || "";
    if(user != "" && user != "undefined"){
      return JSON.parse(user);
    }
    
  }

 
  async getUserFromApi(loginData):Promise<boolean> {
    return new Promise((resolve,reject) => {
      this.api.sendPost('login',loginData).subscribe(async response => {
        await this.setUser(response);
        // this.getMyLeavesFromApi();
        resolve(response);
      }, async (err) => {
        reject(err);
      });
    });
  }

  async createUserFromApi(signupData):Promise<boolean> {
    return new Promise((resolve,reject) => {
      this.api.sendPost('users/',signupData).subscribe(async response => {
        await this.setUser(response);
        resolve(response);
      }, async (err) => {
        reject(err);
      });
    });
  }
   
   
  async getAllUserFromApi():Promise<boolean> {
    return new Promise((resolve,reject) => {
      this.api.sendGet('users').subscribe(async response => {
        await this.setAllUser(response);
        // this.getMyLeavesFromApi();
        resolve(response);
      }, async (err) => {
        reject(err);
      });
    });
  }

  async updateUserFromApi(userData):Promise<boolean> {
    return new Promise((resolve,reject) => {
      this.api.sendPut('users/'+userData.id,userData).subscribe(async response => {
        // await this.setMyLeaves(response);
        resolve(response);
      }, async (err) => {
        reject(err);
      });
    });
  }


}
