import { Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

export const dbTables = {
  USER: "user",
  ALL_USERS: "all_user",
  MY_LEAVES: "my_leaves",
  ALL_LEAVES: "all_leaves",
};

@Injectable({
  providedIn: 'root'
})
export class StoringService {

  
  constructor() { }

  

  keys: string[] = ['user', 'reports'];

  clear(): void {
    window.localStorage.clear();
  }


  clearKey(key: string): boolean {
    localStorage.setItem(key, '');
    return true;
  }


  get(key: string) {
    return localStorage.getItem(key);
  }

  set(key: string, value: any): boolean {
    localStorage.setItem(key, value);
    return false;
  }

}
