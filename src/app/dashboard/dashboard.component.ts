import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { LeaveService } from '../services/leave.service';
import * as moment from 'moment'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public leaves: any[];
  public user: any;
  public users: any;
  public Status: any[];
  public canApply: boolean = false;
  constructor(public userService: UserService,
    public leaveService: LeaveService) { 
      
    }
    
    async ngOnInit() {
      await this.leaveService.getMyLeavesFromApi();
      this.leaves = await this.leaveService.getMyLeaves();
      this.user = await this.userService.getUser();
      this.Status = await this.leaveService.getLeaveStatus();
      
      for (let index = 0; index < this.leaves.length; index++) {
        if(this.leaves[index].status == 0 || this.leaves[index].status == 2 || this.leaves[index].status == 3){
          this.canApply = true
        }
      }
    }
    
    getDiff(start_date, end_date){
      let start = moment(start_date);
      let end = moment(end_date);
      return end.diff(start, 'days');
    }
    
  }
