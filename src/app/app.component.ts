import { Component } from '@angular/core';

import { UserService } from './services/user.service';
import { Router } from '@angular/router';
import { StoringService } from './services/storing.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {
  public user:any;
  public show:boolean;
  constructor(
    public userService: UserService,
    private router: Router,
    public storageService: StoringService) 
    {
      this.user = userService.getUser() || false;
      this.initializeApp();
    }
    
    initializeApp() {
      if(this.user){
        this.show = true;
        this.router.navigate(['/home']);
      }
      else{
        this.router.navigate(['/welcome']);
      }
    }
    
    
    logout(){
      this.storageService.clear();
      this.router.navigate(['/login']);
    }
  }